
/**
 * Wrapper widget for input text. Written since I wanted to show inner label for the input values.
 */
var InputText = Class.define({
    name:"InputText",
    extend:Widget,
    fields:{
        _innerLabel:null,
        _value:null,
        _size:null,
        _keyUpListener:null
    },
    init:function(args){
        InputText.superclass.call(this, args);
        
        this._innerLabel = args.innerLabel || null;
        this._value = args.value || null;
        this._size = args.size || null;
        this._keyUpListener = args.onkeyup || null;
        
    },

    methods:{
        _render:function(elem){
            var inputElem = dom.elem("input", {type:"text", size:this._size, id:this.id+"_input"});

           
            if(this._innerLabel!= null && this._value == null){
                inputElem.value = this._innerLabel;
                css.plusClass(inputElem, 'innerLabel');
                evt.addEventListener(inputElem, "focus", this._clearInnerLabel.closure(this), false);
                evt.addEventListener(inputElem, "blur", this._showInnerLabel.closure(this), false);
            }
            if(this._keyUpListener != null){
                evt.addEventListener(inputElem, "keyup", this._keyUpListener.closure(this));
            }

            if(this._value != null)
                inputElem.value = this._value;
            elem.appendChild(inputElem);
        },

        _clearInnerLabel:function(){
            var inputElem = this.getContainer().firstChild;
            if(inputElem.value == this._innerLabel){
                inputElem.value = "";
                css.minusClass(inputElem, "innerLabel");
            }

        } ,

        _showInnerLabel : function(){
            var inputElem = this.getContainer().firstChild;
            if(inputElem.value == ''){
                inputElem.value = this._innerLabel;
                css.plusClass(inputElem,"innerLabel");
            }
        },



        getValue :function(){
             var inputElem = this.getContainer().firstChild;
            return inputElem.value!=this._innerLabel?inputElem.value:"";
        }
    }
});
