
/**
 * Tabber widget renders a component with tabs.
 * The tabs can be specified as part of the args object and
 * each tab may contain the tab name as the "name" parameter. Additionally
 * it may specify its content as another widget.
 */
var Tabber = Class.define({
    name : "Tabber",

    extend : Widget,

    fields : {
        tabs : null,
        selectedTab : 0,
        availableWidth :0
    },

    init : function(args) {
        Tabber.superclass.call(this, args);
        // copy all provided properties to this widget
        for (var property in args) {
            this[property] = args[property];
        }
    },

    methods:{

        _render : function(elem) {
            var tabberContainer = dom.elem("div");
            var tabberHeader = dom.elem("div", {id:this.getSubId("tabberHeader"), klass:"tabberHeader"});
            tabberContainer.appendChild(tabberHeader);

            var leftDiv = dom.elem("div", {klass:"headerLeft"});
            tabberHeader.appendChild(leftDiv);


           var tabberBody = dom.elem("div", {id:this.getSubId("tabberBody"), klass:"tabberBody"});
           tabberContainer.appendChild(tabberBody);
           
            elem.appendChild(tabberContainer);
            //Cache the available Width for this tabber after rendering it.
            this.availableWidth = css.getOuterWidth(tabberHeader);

            for (var i = 0; i < this.tabs.length; i++) {
                var tab = this._generateTabHeader(this.tabs[i].name, this.selectedTab == i);
                tabberHeader.appendChild(tab);
                this.availableWidth -= this._getWidth(tab);

                var tabContent = this._generateTabBody(this.tabs[i], this.selectedTab == i);
                tabberBody.appendChild(tabContent);

            }
           
            

        },

        _generateTabHeader : function(tabName, isSelected, isCloseable) {
            var spanContainer = dom.elem("span", {id:this.getSubId(tabName,"spanContainer")});
            spanContainer.id = this.id + "_" + tabName + "_spanContainer";
            
            //var className = isSelected ? "tabLeft tabSelectedLeft" :"tabLeft";
            //var spanLeft = dom.elem("span", {klass:className});
            
            var className = isSelected ? "tab tabSelected" : "tab";
            var spanMiddle = dom.elem("span", {klass:className});


            var divName = dom.elem("div", {klass:"tabDiv"});
            divName.innerHTML = tabName;
            
            spanMiddle.appendChild(divName);

            if(isCloseable){
                var attrs = {
                    style : "padding-left:3px",
                    src : "../images/no.png"
                };
                var closeImg = dom.elem("img", attrs);
                spanMiddle.appendChild(closeImg);
            }

            className = isSelected ? "tabRight tabSelectedRight" : "tabRight";
            var spanRight = dom.elem("span", {klass:className});
            

            //spanContainer.appendChild(spanLeft);
            spanContainer.appendChild(spanMiddle);
            spanContainer.appendChild(spanRight);

            evt.addEventListener(spanContainer, "click", this._selectTab.closure(this, tabName), false);

            return spanContainer;
        },

        _generateTabBody : function(tab, isSelected) {
            var tabBody = dom.elem("div", {id:this.getSubId(tab.name, "tabBody")});
            tabBody.style.display = isSelected ? "block" : "none";

            tab.widget.appendTo(tabBody);
            return tabBody;
        },

        addTab : function(tab, isSelected, isCloseable){
            this.tabs.push(tab);
            var tabberHeader = dom.byId(this.id+"_tabberHeader");
            var tabHeader = this._generateTabHeader(tab.name, false, isCloseable);
            tabberHeader.appendChild(tabHeader);

            var tabberBody = dom.byId(this.id+"_tabberBody");
            var tabContent = this._generateTabBody(tab, false);
            tabberBody.appendChild(tabContent);
            
            this._selectTab(tab.name);
            
            //If there is no sufficient width available, then don't add the tab and remove it.
            this.availableWidth -= this._getWidth(tabHeader);
           
            if(this.availableWidth <=0){
                this._removeTab(tab.name);
                alert("No space for more tabs! Please delete any unused tabs.");
            }
            
            
        },

        _selectTab : function(tabName, selectEvt) {
           if(evt.getTargetElem(selectEvt) && evt.getTargetElem(selectEvt).tagName == "IMG"){

               this._removeTab(tabName);
               
               return;
           }
           var id = this.id +"_"+tabName +"_spanContainer";
           var elem = dom.byId(id);

            if (elem) {
                //Unselect all tabs
                var siblings = elem.parentNode.childNodes;
                for (var i = 1; i < siblings.length; i++) {
                    var sChildren = siblings[i].childNodes;
                    //css.minusClass(sChildren[0], "tabSelectedLeft");
                    css.minusClass(sChildren[0], "tabSelected");
                    //css.minusClass(sChildren[2], "tabSelectedRight");
                }

                //Select this tab
                var children = elem.childNodes;
                //css.plusClass(children[0], "tabSelectedLeft");
                css.plusClass(children[0], "tabSelected");
                //css.plusClass(children[2], "tabSelectedRight");
            }


            var tabBodyId = this.id +"_"+tabName+"_tabBody";
            var tabBody = dom.byId(tabBodyId);


            var bodySiblings = tabBody.parentNode.childNodes;
            for(var j=0;j< bodySiblings.length;j++){
                bodySiblings[j].style.display = "none";
            }

            tabBody.style.display = "block";

        },

        _removeTab:function(tabName){
            var index;
                for(var i=0;i<this.tabs.length;i++){
                    if(this.tabs[i].name ==tabName){
                        index = i;
                        break;
                    }
                }
            if(index){
                this.tabs.splice(i,1);
                 var tabberHeader = dom.byId(this.getSubId("tabberHeader"));
                 var tabberBody = dom.byId(this.getSubId("tabberBody"));
                 var tabWidth = this._getWidth(dom.byId(this.getSubId(tabName, "spanContainer")));
                 tabberHeader.removeChild(dom.byId(this.getSubId(tabName, "spanContainer")));
                 tabberBody.removeChild(dom.byId(this.getSubId(tabName, "tabBody")));
                 //Add the removed Tab's width from the available width.
                 this.availableWidth += tabWidth;
               
                //IE8 has issues with handling the direct call..:(
                setTimeout(this._selectTab.closure(this, this.tabs[this.tabs.length-1].name, false),1);
            }

        },

        hasTab:function(tabName){
            for(var i=0;i<this.tabs.length;i++){
                if(this.tabs[i].name == tabName){
                    return true;
                }
            }

            return false;
        },

        _scrollLeft:function(){
            
        },

        _scrollRight:function(){
            
        },
        
        _getWidth : function(tabHeader){
            var nodes = tabHeader.childNodes;
            var width = 0;
            for(var i=0;i<nodes.length;i++){
                width += css.getOuterWidth(nodes[i]);  
            }
           
            return width;
        }


    }

});
