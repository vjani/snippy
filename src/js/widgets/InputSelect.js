
var InputSelect  = Class.define({
    name:"InputSelect",

    extend:Widget,

    fields:{
        value:null,
        values:null,
        innerLabel:null
    },

    init : function(args){
        InputSelect.superclass.call(this, args);
        this.values = args.values || null;
        this.value = args.value ||null;
        this.innerLabel = args.innerLabel || null;
    },

    methods:{
        
        _render : function(elem){
            var selectElem = dom.elem("select", { id:this.id+"_select"});

            if(this.innerLabel){
                selectElem.appendChild(this._createOption({label:this.innerLabel, value:this.innerLabel}));
                
            }
            if(this.values){
                for(var i=0;i<this.values.length;i++){
                    selectElem.appendChild(this._createOption(this.values[i]));
                }
            }
           
            elem.appendChild(selectElem);
        
        },
        
        _createOption : function(opt){
            var option = dom.elem("option", {value:opt.value});
            option.innerHTML = opt.label;
            option.selected = opt.isSelected || this.value == opt.value;
            return option;
        },
        
        getValue :function(){
             var selectElem = dom.byId(this.id+"_select");
            return selectElem.value;
        }

    }

});
