
/**
 * Widget class is the root for every widget.
 * It defines some basic methods for rendering simple widgets.
 * It creates a container for the widget and calls the _render method
 * which can be over-ridden by the subclasses.
 */
var Widget = Class.define({
    name : "Widget",
    init : function(args) {
        if (args.id) {
            this.id = args.id;
            core.cacheWidget(this);
        }
        if (args.cssClass) {
            this._cssClass = args.cssClass;
        }
        if (args.cssStyle) {
            this._cssStyle = args.cssStyle;
        }
        //User may pass html or node but not both
        if (args.html) {
            this._html = args.html;
        } else if(args.node){
            this._childNode = args.node;
        }
    },

    fields : {
        _cssClass : null,
        _cssStyle : null,
        id : null,
        _html : null,
        _childNode :null
    },

    methods : {

        _render : function(elem) {
            elem.innerHTML = this._html != null ? this._html : "";
            if(this._childNode != null){
                elem.appendChild(this._childNode);
            }
        },

        appendTo : function(parentElem) {
            if (this.getContainer() != null)
                throw new Error("container already exists");
            var elem = this._createContainer();
            parentElem.appendChild(elem);
            this._render(elem);
            return elem;

        },

        getContainer : function() {
            return dom.byId(this.id);
        },

        _createContainer : function() {
			var attrs = {id: this.id};
			if(this._cssStyle) attrs.style = this._cssStyle;
			if(this._cssClass) attrs.klass = this._cssClass;
            return dom.elem("div", attrs);
        },

        reRender : function(){
            var elem = this.getContainer();
            core.removeAllChildren(elem);
            this._render(elem);
        },
        
        getSubId : function(){
            var subId = this.id;
            for(var i=0;i<arguments.length;i++){
                subId += "_"+arguments[i];
            }
            return subId;
        },

        addEvents : function(){
            throw Error("Implement this if you are calling it!");
        }
    }
});




