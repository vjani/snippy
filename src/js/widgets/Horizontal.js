

/**
 * Horizontal renders the given children horizontally. Currently it is a basic widget and
 * uses tables for horizontal layout,which might not be ideal(as per semantic markup).But
 * the table's "role" attribute is presentation, which will not declare it as table for accessible users.
 */
var Horizontal = Class.define({
    name : "Horizontal",

    extend : Widget,

    fields : {
        children : null
    },

    init : function(args) {
        Horizontal.superclass.call(this, args);
        if (args.children != null) {
            this.children = args.children;
        }
    },

    methods : {
        _render : function(elem) {
            var table = dom.elem("table", {role:"presentation"});
			var tbody = dom.elem("tbody");
            var tr = dom.elem("tr");
            for (var i = 0; i < this.children.length; i++) {
                var td = dom.elem("td");
                this.children[i].appendTo(td);
                //this._modifyContainer(this.children[i]);
                tr.appendChild(td);
            }
            tbody.appendChild(tr);
			table.appendChild(tbody);
            elem.appendChild(table);
        }
    }
});
