var Class = {
    /**
     * Defines a class with the given properties and methods.
     *
     * @param args - Bag of arguments which contains this class's properties. It may
     * contain the following:
     *
     * fields - Object containing private fields.
     * methods - Object containing private methods.
     * extend - Superclass for this class.
     * init - constructor function for the class.
     * name - This class's name.
     */
    define : function(args) {
        var classname = args.name;
        var superclass = args.extend || Object;
        var methods = args.methods || [];
        var fields = args.fields || [];
        var init = args.init || function() {
        };

        var constructor = function() {
            if (superclass)
                superclass.apply(this, arguments); // Initialize superclass
            if (init)
                init.apply(this, arguments); // Initialize ourself
        };


        constructor.superclass = superclass || Object;
        constructor.classname = classname;

        // Create an instance of the superclass to use as the prototype for
        // the new class. Assign it to constructor.prototype
        var proto = constructor.prototype = new superclass({});

        // copy the methods to the class.
        for (var i in methods)
            proto[i] = methods[i];
        for (var j in fields)
            proto[j] = fields[j];

        proto.constructor = constructor;
        return constructor;
    }

};
