
var highlighter = {
    
    langs : {
       JS:"Javascript",
       C:"C",
       CPP:"C++",
       Java:"Java",
       Python:"Python",
       Php:"Php",
       Perl:"Perl",
       Ruby:"Ruby",
       ObjC:"Objective C",
       Scala:"Scala"
       
    },
    
    
    format : function(args){
      
       switch(args.type){
        case this.langs.JS: return this.formatJS(args.code);
        case this.langs.C : return this.formatC(args.code);
        case this.langs.CPP: return this.formatCpp(args.code);
        case this.langs.Java: return this.formatJava(args.code);
        case this.langs.Python:return this.formatPython(args.code);
        case this.langs.Php: return this.formatPhp(args.code);
        case this.langs.Perl: return this.formatPerl(args.code);
        case this.langs.Ruby: return this.formatRuby(args.code);
        case this.langs.ObjC: return this.formatObjC(args.code);
        case this.langs.Scala: return this.formatScala(args.code);
        default : return args.code;
       }
    },
    
    

    process : function(args){
        var code = args.code.replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
        var results = [];
        results.push("<ol><li>");
        var singleQuoteRegExp = "(\"[^\"]*\")";
        var doubleQuoteRegExp = "('[^']*')";
        var syntaxRegExp = new RegExp("(\\n)|"
                + args.multiCommentRegExp + "|"
                + args.singleCommentRegExp + "|"
                + doubleQuoteRegExp + "|"
                + singleQuoteRegExp + "|"
                + args.keywordsRegExp + "|"
                + args.builtInObjRegExp , "g");

        results.push(code.replace(syntaxRegExp,
                        function(m, matchNewLine,  matchMultiComment, dummyMulti ,matchSingleComment, matchDoubleQ, matchSingleQ, matchKeyword, matchBuiltIn){
                          
                            if(matchNewLine != null && matchNewLine != ""){
                                return "</li><li>";
                            }
                            else if(matchMultiComment != null && matchMultiComment != ""){

                                var res = "<span class='multiComments'>"+matchMultiComment+"</span>";
                                res = res.replace(/\n/g, "</span></li><li><span class='multiComments'>");
                                return res;

                            }
                            else if (matchSingleComment != null && matchSingleComment != ""){
                                return "<span class='comments'>"+matchSingleComment +"</span>";
                            }
                            else if(matchDoubleQ != null && matchDoubleQ!=""){
                                return "<span class='quotes'>"+matchDoubleQ+"</span>";
                            } else if(matchSingleQ != null && matchSingleQ != ""){
                                return "<span class='quotes'>"+matchSingleQ+"</span>";
                            } else if(matchKeyword != null && matchKeyword != ""){
                                return "<span class='keyword'>"+matchKeyword+"</span>";
                            }  else if(matchBuiltIn != null && matchBuiltIn != ""){
                                return "<span class='builtInClass'>"+matchBuiltIn+"</span>";
                            }
                        }));
       results.push("</ol>");
       return results.join("");
    },

    formatJS :function(code){
        var args = {
            code:code,
            multiCommentRegExp:"(\/\\*([^*]|\\*[^\/])*\\*\/)",
            singleCommentRegExp:"(\/\/[^\\n]*)",
            keywordsRegExp:"\\b(abstract|boolean|break|byte|case|catch|class|char|const|continue|debugger|default|delete|do|double|else|enum|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with)\\b",
            builtInObjRegExp:"\\b(history|prototype|window|document|navigator|Object|Function|Array|String|Boolean|Number|Math|Date|RegExp)\\b"
        };

        return this.process(args);
    },

    formatC:function(code){
         var args = {
            code:code,
            multiCommentRegExp:"(\/\\*([^*]|\\*[^\/])*\\*\/)",
            singleCommentRegExp:"(\/\/[^\\n]*)",
            keywordsRegExp:"\\b(auto|break|case|char|const|continue|default|do|double|else|enum|extern|float|for|goto|if|int|long|register|return|short|signed|sizeof|static|struct|switch|typedef|union|unsigned|void|volatile|while)\\b",
            builtInObjRegExp:"\\b(scanf|printf|fscanf|fprintf|malloc|calloc|strcpy|strncpy|strcmp|strlen|strcat|strchr|strrchr|strstr|strtok|memcpy|memcmp|memset|memmove|isalnum|isalpha|iscntrl|isdigit|isgraph|islower|isupper|isprint|isspace|ispunct|isxdigit|tolower|toupper|perror|strerror)\\b"
        };

        return this.process(args);
    },

    formatCpp:function(code){
        var args = {
            code:code,
            multiCommentRegExp:"(\/\\*([^*]|\\*[^\/])*\\*\/)",
            singleCommentRegExp:"(\/\/[^\\n]*)",
            keywordsRegExp:"\\b(and|and_eq|asm|auto|bitand|bitor|bool|break|case|catch|char|class|compl|const|const_cast|continue|default|delete|do|double|dynamic_cast|else|enum|explicit|extern|false|float|for|friend|goto|if|inline|int|long|mutable|namespace|new|not|not_eq|operator|or|or_eq|private|protected|public|register|reinterpret_cast|return|short|signed|sizeof|static|static_cast|struct|switch|template|this|throw|true|try|typedef|typeid|typename|union|unsigned|using|virtual|void|volatile|wchar_t|while|xor|xor_eq)\\b",
            builtInObjRegExp:"\\b(scanf|printf|fscanf|fprintf|malloc|calloc|strcpy|strncpy|strcmp|strlen|strcat|strchr|strrchr|strstr|strtok|memcpy|memcmp|memset|memmove|isalnum|isalpha|iscntrl|isdigit|isgraph|islower|isupper|isprint|isspace|ispunct|isxdigit|tolower|toupper|perror|strerror)\\b"
        };

        return this.process(args);
    },

    formatJava:function(code){
        var args = {
            code:code,
            multiCommentRegExp:"(\/\\*([^*]|\\*[^\/])*\\*\/)",
            singleCommentRegExp:"(\/\/[^\\n]*)",
            keywordsRegExp:"\\b(abstract|assert|boolean|break|byte|case|catch|char|class|const|continue|default|do|double|else|enum|extends|final|finally|float|for|goto|if|implements|import|instanceof|int|interface|long|native|new|package|private|protected|public|return|short|static|strictfp|super|switch|synchronized|this|throw|throws|transient|try|void|volatile|while)\\b",
            builtInObjRegExp:"\\b(String|Boolean|Integer|Float|Double|Object|Class|Byte|Char|Long|Short|Date|Time|Collection|List|Iterator|Set|Map)\\b"
        };

        return this.process(args);
    },

    formatPython:function(code){
        var args = {
            code:code,
            multiCommentRegExp:"(\"\"\"([^\"]*)\"\"\")",
            singleCommentRegExp:"(#[^\\n]*)",
            keywordsRegExp:"\\b(and|assert|break|class|continue|def|del|elif|else|except|exec|finally|for|from|global|if|import|in|is|lambda|not|or|pass|print|raise|return|try|while|yield)\\b",
            builtInObjRegExp:"\\b(abs|all|any|basestring|bin|bool|callable|chr|classmethod|cmp|compile|complex|delattr|dict|dir|divmod|enumerate|eval|execfile|file|filter|float|format|frozenset|getattr|globals|hasattr|hash|help|hex|id|input|int|isinstance|issubclass|iter|len|list|locals|long|map|max|min|next|object|oct|open|ord|pow|print|property|range|raw_input|reduce|reload|repr|reversed|round|set|setattr|slice|sorted|staticmethod|str|sum|super|tuple|type|unichr|unicode|vars|xrange|zip|__import__|apply|buffer|coerce)\\b"
        };

        return this.process(args);
    },

    //TODO: php syntax highlighting might have issues when it is mixed with HTML/CSS
    formatPhp:function(code){
          var args = {
            code:code,
            multiCommentRegExp:"(\/\\*([^*]|\\*[^\/])*\\*\/)",
            singleCommentRegExp:"(#[^\\n]*|\/\/[^\\n]*)",
            keywordsRegExp:"\\b(abstract|and|array|as|break|case|catch|cfunction|class|clone|const|continue|declare|default|do|else|elseif|enddeclare|endfor|endforeach|endif|endswitch|endwhile|extends|final|for|foreach|function|global|goto|if|implements|interface|instanceof|namespace|new|old_function|or|private|protected|public|static|switch|throw|try|use|var|while|xor)\\b",
            builtInObjRegExp:"\\b(die|echo|empty|exit|eval|include|include_once|isset|list|require|require_once|return|print|unset|self|static|parent)\\b"
        };

        return this.process(args);
    },

    //Phew, Perl is a strange language, 252 keywords and no official way of multi comments!!!
    formatPerl:function(code){
        var args = {
            code:code,
            multiCommentRegExp:"(dfdfdfds(dsfdsfds)dsfsdfad)", //TODO:dummy string to not match practically anything, is there a way to not match and always return null??
            singleCommentRegExp:"(#[^\\n]*)",
            keywordsRegExp:"\\b(abs|accept|alarm|and|atan2|bind|binmode|bless|caller|chdir|chmod|chomp|chop|chown|chr|chroot|close|closedir|cmp|connect|continue|cos|crypt|dbmclose|dbmopen|defined|delete|die|do|dump|each|else|elsif|endgrent|endhostent|endnetent|endprotoent|endpwent|endservent|eof|eq|eval|exec|exists|exit|exp|fcntl|fileno|flock|for|foreach|fork|format|formline|ge|getc|getgrent|getgrgid|getgrnam|gethostbyaddr|gethostbyname|gethostent|getlogin|getnetbyaddr|getnetbyname|getnetent|getpeername|getpgrp|getppid|getpriority|getprotobyname|getprotobynumber|getprotoent|getpwent|getpwnam|getpwuid|getservbyname|getservbyport|getservent|getsockname|getsockopt|glob|gmtime|goto|grep|gt|hex|if|index|int|ioctl|join|keys|kill|last|lc|lcfirst|le|length|link|listen|local|localtime|lock|log|lstat|lt|m|map|mkdir|msgctl|msgget|msgrcv|msgsnd|my|ne|next|no|not|oct|open|opendir|or|ord|our|pack|package|pipe|pop|pos|print|printf|prototype|push|q|qq|qr|quotemeta|qu|qw|qx|rand|read|readdir|readline|readlink|readpipe|recv|redo|ref|rename|require|reset|return|reverse|rewinddir|rindex|rmdir|s|scalar|seek|seekdir|select|semctl|semget|semop|send|setgrent|sethostent|setnetent|setpgrp|setpriority|setprotoent|setpwent|setservent|setsockopt|shift|shmctl|shmget|shmread|shmwrite|shutdown|sin|sleep|socket|socketpair|sort|splice|split|sprintf|sqrt|srand|stat|study|sub|substr|symlink|syscall|sysopen|sysread|sysseek|system|syswrite|tell|telldir|tie|tied|time|times|tr|truncate|uc|ucfirst|umask|undef|unless|unlink|unpack|unshift|untie|until|use|utime|values|vec|wait|waitpid|wantarray|warn|while|write|x|xor|y)\\b",
            builtInObjRegExp:"\\b(NULL|__FILE__|__LINE__|__PACKAGE__|__DATA__|__END__|AUTOLOAD|BEGIN|CORE|DESTROY|END|EQ|GE|GT|INIT|LE|LT|NE|CHECK)\\b"
        };

        return this.process(args);
    },

    formatRuby:function(code){
        var args = {
            code:code,
            multiCommentRegExp:"(=begin([^=]*)=end)",
            singleCommentRegExp:"(#[^\\n]*)",
            keywordsRegExp:"\\b(alias|and|BEGIN|begin|break|case|class|def|defined|do|else|elsif|END|end|ensure|false|for|if|in|module|next|nil|not|or|redo|rescue|retry|return|self|super|then|true|undef|unless|until|when|while)\\b",
            builtInObjRegExp:"\\b(Array|Float|Integer|String|at_exit|autoload|binding|caller|catch|chop|chop!|chomp|chomp!|eval|exec|exit|exit!|fail|fork|format|gets|global_variables|gsub|gsub!|iterator?|lambda|load|local_variables|loop|open|p|print|printf|proc|putc|puts|raise|rand|readline|readlines|require|select|sleep|split|sprintf|srand|sub|sub!|syscall|system|test|trace_var|trap|untrace_var)\\b"
        };

        return this.process(args);
    },

    formatObjC:function(code){
        var args = {
            code:code,
            multiCommentRegExp:"(\/\\*([^*]|\\*[^\/])*\\*\/)",
            singleCommentRegExp:"(\/\/[^\\n]*)",
            keywordsRegExp:"\\b(interface|implementation|protocol|in|out|inout|bycopy|byref|oneway|alloc|retain|release|autorelease|end|class|selector|protocol|encode|and|and_eq|asm|auto|bitand|bitor|bool|break|case|catch|char|class|compl|const|const_cast|continue|default|delete|do|double|dynamic_cast|else|enum|explicit|extern|false|float|for|friend|goto|if|inline|int|long|mutable|namespace|new|not|not_eq|operator|or|or_eq|private|protected|public|register|reinterpret_cast|return|short|signed|sizeof|static|static_cast|struct|switch|template|this|throw|true|try|typedef|typeid|typename|union|unsigned|using|virtual|void|volatile|wchar_t|while|xor|xor_eq)\\b",
            builtInObjRegExp:"\\b(scanf|printf|fscanf|fprintf|malloc|calloc|strcpy|strncpy|strcmp|strlen|strcat|strchr|strrchr|strstr|strtok|memcpy|memcmp|memset|memmove|isalnum|isalpha|iscntrl|isdigit|isgraph|islower|isupper|isprint|isspace|ispunct|isxdigit|tolower|toupper|perror|strerror)\\b"
        };

        return this.process(args);
    },

    formatScala:function(code){
        var args = {
            code:code,
            multiCommentRegExp:"(\/\\*([^*]|\\*[^\/])*\\*\/)",
            singleCommentRegExp:"(\/\/[^\\n]*)",
            keywordsRegExp:"\\b(abstract|case|catch|class|def|do|else|extends|false|final|finally|for|forSome|if|implicit|import|lazy|match|new|null|object|override|package|private|protected|requires|return|sealed|super|this|throw|trait|try|true|type|val|var|while|with|yield)\\b",
            builtInObjRegExp:"\\b(Any|AnyVal|AnyRef|Boolean|Int|Float|Double|Byte|Char|Long|Short|Collection|Either|Function|Iterable|List|Nothing|Null|Option|Predef|Product|ScalaObject|Seq|Tuple)\\b"
        };

        return this.process(args);
    },

    formatSmallTalk:function(code){

    },

    formatAssembly:function(code){

    },

    formatScheme:function(code){

    },

    formatSQL:function(code){

    }

}

