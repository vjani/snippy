//This file contains utility methods for the core jobs.


var dom = {
    
    /**
     * Creats a new dom element with the given bag of attributes.
     */
    elem : function(tagName, attrs){
        var elem = document.createElement(tagName);
        if(attrs) {
            for(var attr in attrs){
                if(attr == "style"){
                    elem.style.cssText = attrs[attr];  
                } else if(attr == "klass"){
                    elem.className = attrs[attr];
                } else 
                    elem.setAttribute(attr, attrs[attr]);
            }
        }
        return elem;
        
    },
    
    byId : function(id, doc){
        if(!id || id == null){
            return null;
        }
        return doc? doc.getElementById(id) : document.getElementById(id);
    }
}
/**
 * css object contains convenient methods for finding the correct innerheight/outerheight
 * and setting/removing classnames.
 */
var css = {

    getInnerWidth : function(elem) {
        var style = css.getComputedStyle(elem);
        var w = elem.offsetWidth;
        // Subtract the border left/right and padding left/right to get the real
        // content width.
        w -= css.parseInt(style.borderLeftWidth)
                + css.parseInt(style.borderRightWidth);
        w -= css.parseInt(style.paddingLeft) + css.parseInt(style.paddingRight);
        return w;
    },

    getOuterWidth : function(elem) {
        return elem.offsetWidth;
    },

    setInnerWidth : function(elem, val) {
        elem.style.width = val + "px";
    },

    setOuterWidth : function(elem, val) {
        var style = css.getComputedStyle(elem);
        val -= css.parseInt(style.borderLeftWidth)
                + css.parseInt(style.borderRightWidth);
        val -= css.parseInt(style.paddingLeft)
                + css.parseInt(style.paddingRight);
        if (val < 0)
            val = 0;
        elem.style.width = val + "px";
    },

    getInnerHeight : function(elem) {
        var style = css.getComputedStyle(elem);
        var h = elem.offsetHeight;
        // Subtract the border top/bottom and padding top/bottom to get the real
        // content height.
        h -= css.parseInt(style.borderTopWidth)
                + css.parseInt(style.borderBottomWidth);
        h -= css.parseInt(style.paddingTop) + css.parseInt(style.paddingBottom);
        return h;
    },

    getOuterHeight : function(elem) {
        return elem.offsetHeight;
    },

    setInnerHeight : function(elem, val) {
        elem.style.height = val + "px";
    },

    setOuterHeight : function(elem, val) {
        var style = css.getComputedStyle(elem);
        val -= css.parseInt(style.borderTopWidth)
                + css.parseInt(style.borderBottomWidth);
        val -= css.parseInt(style.paddingTop)
                + css.parseInt(style.paddingBottom);
        if (val < 0)
            val = 0;
        elem.style.height = val + "px";
    },

    getComputedStyle : function(elem, property) {
        var style;
        if (core.isIE) {
            style = elem.currentStyle;
        } else {
            style = elem.ownerDocument.defaultView.getComputedStyle(elem, "");
        }
        return (style && property) ? style[property] : style;
    },

    parseInt : function(cssVal) {
        var num = parseInt(cssVal);
        return isNaN(num) ? 0 : num;
    },

    plusClass : function(elem, cssClass) {
        if (elem) {
            var classNames = elem.className == null ? "" : elem.className;
            var space = " ";
            var newClassNames = space + classNames + space;
            if (newClassNames.indexOf(space + cssClass + space) == -1) {
                newClassNames += cssClass;
                newClassNames = newClassNames.substring(1);//Remove the extra space at 1st position.
                if(classNames ==""){
                    newClassNames = newClassNames.substring(1);//In this case there will be 2 spaces to remove.
                }
            }


            if (classNames != newClassNames) {
                elem.className = newClassNames;
            }
        }
    },

    minusClass : function(elem, cssClass) {
        if (elem) {
            var classNames = (elem.className == null) ? "" : elem.className;
            if (classNames == "") return;
            var space = " ";
            var newClassNames = classNames;
            var position = (space + classNames + space).indexOf(space + cssClass + space);
            if (position == 0) {
                newClassNames = classNames.substring(cssClass.length + space, classNames.length);
            } else if (position > 0) {
                var begin = classNames.substring(0, position - space.length);
                var end = classNames.substring(position + cssClass.length, classNames.length);
                newClassNames = begin + end;
            }

            if (classNames != newClassNames) {
                elem.className = newClassNames;
            }
        }
    }
};

/**
 * evt provides methods for adding/removing event handlers.
 */
var evt = {
    addEventListener : function(elem, type, handler, capture) {
        if (core.isIE) {
            elem.attachEvent("on" + type, handler);
        } else {
            elem.addEventListener(type, handler, capture);
        }
    },

    removeEventListener : function(elem, type, handler, capture) {
        if (core.isIE) {
            elem.detachEvent("on" + type, handler);
        } else {
            elem.removeEventListener(type, handler, capture);
        }
    },

    getTargetElem :function(browserEvent) {
        if(core.isIE && window.event){
            return window.event.srcElement;
        }
        return browserEvent? browserEvent.target:null;
    }

};
/*
 *core provides various methods like getXMLHttpRreques, browser sniffing etc.
 */
var core = {
    _widgets :{},
    // Browser sniffing
    // ------------------------------------------------------------------
    _sniffBrowser : function() {
        // NOTE: This function is very rough, for testing on supported browsers
        // only. It
        // is not production-ready
        var rawUA = navigator.userAgent.toLowerCase();
        core.isSafari = rawUA.indexOf("safari") >= 0;
        core.isGecko = rawUA.indexOf("gecko") >= 0 && !core.isSafari;
        core.isMozilla = core.isGecko; // temp, until we decide which to use
        core.isIE = !core.isGecko && !core.isSafari;

        core.isMac = (rawUA.indexOf('mac') > -1);
        core.isWin = (rawUA.indexOf('win') > -1);

    },

    getXMLHttpRequest : function() {
        var httpRequest;

        try {
            httpRequest = new XMLHttpRequest();
        } catch (ex) {
            alert(ex);
        }

        if (httpRequest == null) {
            // Get the XMLHttpRequest instance from the latest version of the MSXML library
            try {
                httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
            } catch (ex) {
                alert(ex);
            }
        }

        return httpRequest;
    },

    cacheWidget : function(widget){
        this._widgets[widget.id]= widget;
    },

    findWidget:function(id){
        return this._widgets[id];
    },

     removeAllChildren : function(elem) {
        if(core.isIE){
            while(elem.firstChild) {
                elem.removeChild(elem.firstChild);
            }
        }else {
            elem.innerHTML = "";
        }
    },

    trim : function(str){
        return str.replace(/^\s*/, "").replace(/\s*$/, "");
    }
};

/**
 * This extension to the Function object helps to register the method calls, complete with the object that they
 * are supposed to be invoked on, the arguments to be passed and even the new arguments to be passed when it is actually
 * invoked at a later point of time. very useful for event handlers.
 * @param cThis
 */
Function.prototype.closure = function(cThis) {
    var fn = this;
    var closureArgs = null;
    if (arguments.length > 1) {
        // 'arguments' isn't a real array - must copy manually
        closureArgs = new Array(arguments.length - 1);
        for (var i = 1; i < arguments.length; i++) {
            closureArgs[i - 1] = arguments[i];
        }
        return function() {
            var args;
            if (arguments.length == 0) {
                // no additional arguments were passed, call with just the arguments fixed by the closure
                args = closureArgs;
            } else {
                // additional arguments were passed, add them to the arguments fixed by the closure
                args = new Array();
                for (var i = 0; i < closureArgs.length; i++) {
                    args.push(closureArgs[i]); // copy the arguments fixed by the closure
                }
                for (var j = 0; j < arguments.length; j++) {
                    args.push(arguments[j]); // copy additional arguments
                }
            }
            return fn.apply(cThis, args);
        }
    } else {
        return function() {
            //if(arguments == null) ...
            return fn.apply(cThis, arguments);
        }
    }
};

/**
 * requestMgr handles sending the ajax requests and serializing js objects as strings (representing json) when they are
 * sent
 */
var requestMgr = {

    submitRequest : function(action, actionParams, callback, statusText){
        var url = "/snippetz";
        statusText = statusText!=null?statusText:"Loading...";
        var statusElem = dom.byId("statusText");
        statusElem.innerHTML = statusText;
        statusElem.style.display = 'block';
        var request = core.getXMLHttpRequest();
        request.open("POST", url, true);
        request.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");
        request.onreadystatechange = function() {
            if (request.readyState == 4) {
                 var statusElem = dom.byId("statusText");
                 statusElem.style.display = 'none';
                var actionResult;
                if (200 == request.status) {
                    try {
                        actionResult = JSON.parse(request.responseText);
                    }
                    catch (e) {
                       alert(e);
                        return;
                    }
                } else {
                    alert("An error occurred. Request couldn't be completed");
                    return;
                }

                if (callback) {

                    callback(actionResult);
                }
            }
        };


        // package the request parameters to be sent to the server
        var requestParams = {action: action, actionParams: actionParams};


        request.send(this.serializedRequestParameter(requestParams));

    },

    serializedRequestParameter : function(requestParams) {
        var s = [];
        //create a token of the request paramter name value pair
        for (var j in requestParams) {
            if ("actionParams" == j) {
                s.push(j + "=" + encodeURIComponent(JSON.stringify(requestParams[j])));

            } else {
                s.push(j + "=" + encodeURIComponent(requestParams[j]));
            }
        }
        //join everything together into this format name=value&name2=value2&...
        return s.join("&");
    }
};

String.prototype.trim = function () {
    return this.replace(/^\s*/, "").replace(/\s*$/, "");
}


//Use the advance highlighter now. But just keep this just in case...

//var highlighter = {
//
//    /**
//     * Basic syntax highlighter for javascript code.
//     * It just highlights the single-line comments, multi-line comments, keywords and some built-in objects.
//     * It does line-by-line parsing, figures out which are comments/multi-line comments or normal code. Finally, it
//     * applies the css classes to each line based on its type. This highlighter might have issues if comments are
//     * contained within quotes.
//     * @param code
//     */
//    process : function (code) {
//        var lines = code.split("\n");
//        var processedLines = [];
//        var thisLineProcessed = false;
//        var isCommentContinued = false;
//        for (var i = 0; i < lines.length; i++) {
//            thisLineProcessed = false;
//            if (lines[i].indexOf("/*") != -1 || lines[i].indexOf("*/") != -1) {
//                isCommentContinued = this.captureMulti(lines[i], processedLines);
//                thisLineProcessed = true;
//            }
//
//            if (thisLineProcessed) continue;
//            if (!isCommentContinued) {
//                this.captureSingle(lines[i], processedLines);
//            } else {
//                processedLines.push({frag:lines[i] + '\n', type:"multiple"})
//            }
//        }
//        var formattedString = [];
//        //Skip the first line, since that will be a dummy one with nothing but a \n
//        for (var j = 0; j < processedLines.length; j++) {
//            if(j==0 && processedLines[j].frag=="\n") continue;
//            if (processedLines[j].type == 'single') {
//                processedLines[j].frag = processedLines[j].frag.replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
//                processedLines[j].frag = "<span class='comments'>" + processedLines[j].frag; //+ "</span>"; Close this span later when we add the closing <li>
//            } else if (processedLines[j].type == 'multiple') {
//                processedLines[j].frag = processedLines[j].frag.replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
//                processedLines[j].frag = "<span class='multiComments'>" + processedLines[j].frag; //+ "</span>"; Close this span later when we add the closing <li>
//            } else if (processedLines[j].type == 'normal') {
//                processedLines[j].frag = processedLines[j].frag.replace(/\&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
//                processedLines[j].frag = processedLines[j].frag.replace(/("[^"]*")|('[^']*')/g, "<span class='quotes'>\$1\$2</span>");
//                processedLines[j].frag = processedLines[j].frag.replace(/\b(abstract|boolean|break|byte|case|catch|char|const|continue|debugger|default|delete|do|double|else|enum|export|extends|false|final|finally|float|for|function|goto|if|implements|import|in|instanceof|int|interface|long|native|new|null|package|private|protected|public|return|short|static|super|switch|synchronized|this|throw|throws|transient|true|try|typeof|var|void|volatile|while|with)\b/g,
//                        "<span class='keyword'>\$1</span>");
//                processedLines[j].frag = processedLines[j].frag.replace(/\b(history|prototype|window|document|navigator|Object|Function|Array|String|Boolean|Number|Math|Date|RegExp)\b/g, "<span class='builtInClass'>\$1</span>");
//            }
//            formattedString.push(processedLines[j].frag.replace("\n", "</span></li>"+(j!=processedLines.length-1?"<li>":"")));
//        }
//        return "<ol><li>" + formattedString.join("") + "</ol>";
//
//    },
//    //This is a recursive function for capturing /*..*/ comments within one line.
//    captureMulti:function (line, k) {
//        if (line.indexOf("/*") != -1) {
//            //push the 1st part as normal
//            this.captureSingle(line.substring(0, line.indexOf("/*")),k);
//            //Get the remaining part and search a closing comment there
//            line = line.substring(line.indexOf("/*"), line.length);
//            if (line.indexOf("*/") != -1) {
//                k.push({frag:line.substring(0, line.indexOf("*/") + 2), type:'multiple'});
//                line = line.substring(line.indexOf("*/") + 2, line.length);
//                return this.captureMulti(line, k);
//            } else {
//                //push the 2nd part as comment;
//                k.push({frag:line + "\n",type:'multiple'});
//
//                return true;
//            }
//        } else if (line.indexOf("*/") != -1) {
//            //push the 1st part as commented
//            k.push({frag:line.substring(0, line.indexOf("*/") + 2),type:'multiple'});
//            line = line.substring(line.indexOf("*/") + 2, line.length);
//            return this.captureMulti(line, k);
//
//        } else {
//            this.captureSingle(line, k);
//            return false;
//        }
//    },
//
//    captureSingle : function(line, k) {
//        if (line.indexOf("//") != -1) {
//            var normalPart = line.substring(0, line.indexOf("//"));
//            var commentedPart = line.substring(line.indexOf("//"), line.length);
//            k.push({frag:normalPart,type:"normal"});
//            k.push({frag:commentedPart + "\n", type:"single"});
//        } else {
//            k.push({frag:line + "\n",type:"normal"});
//        }
//    }
//};

core._sniffBrowser();



