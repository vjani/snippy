/*
 * snippetz has the application logic and the code for contacting the backend. Most of
 * the render logic is part of the widgets.
 */
var snippetz = {
    _cachedTags:{},
	init : function(){

	    this.browseSnippets = new SnippetResults({
            id:'browseSnippets', cssStyle:"padding-top:10px",
            resultsFetcher:snippetz.resultsFetcher.closure(snippetz)
        });

        this.browseTags = new SnippetResults({
             id:'browseTags', cssStyle:'padding-top:10px', fetchTags:true,
            tagClickHandler:snippetz.tagClickHandler.closure(snippetz),
            resultsFetcher:snippetz.tagsFetcher.closure(snippetz)
        });

		this.tabbedPane = new Tabber({
			id:"tabbedPane",
			selectedTab:0,
			tabs:[{name:"Browse snippets", widget:this.browseSnippets},
			      {name:"Browse tags", widget:this.browseTags}]
		});

		this.tabbedPane.appendTo(dom.byId("main"));
		
		this.searchInput = new InputText({
			id:"searchBarInput",
			innerLabel:"Search...", size:30, cssClass:"searchBar",
			onkeyup:this.enterKeyListener.closure(this)
		});
		
		this.searchInput.appendTo(dom.byId("searchBar"));
	},

    resultsFetcher : function(beginIndex, endIndex, callback){
            requestMgr.submitRequest("BROWSE", {"beginIndex":beginIndex, "endIndex":endIndex}, this.resultsFetcherCallback.closure(this, callback));
    },

    resultsFetcherCallback : function(callback, results){
        callback(results);
    },

    tagsFetcher : function(beginIndex, endIndex, callback){
             requestMgr.submitRequest("BROWSE_TAGS", {}, this.tagsFetcherCallback.closure(this, callback));
    },

    tagsFetcherCallback : function(callback, results){
        if(results.result){
            for(var i=0;i<results.result.length;i++){
                this._cachedTags[results.result[i].id]=results.result[i];
            }
        }
        callback(results);
    },

    getCreateWidget:function(){
        var createWidget = new Widget({
            id:"createWidget",
            html:"<pre class='editor'></pre>"
        });
    },

    openCreateTab : function(){
        if(!this.tabbedPane.hasTab("Create Snippet")) {
            this.codeEditor = new CodeEditor({id:"createSnip", code:"", isEdit:true});
            this.tabbedPane.addTab({name:"Create Snippet", widget:this.codeEditor}, true, true);
        }
        else{
            this.tabbedPane._selectTab('Create Snippet');
        }
    },

    saveSnippet :function(){
        var values = this.codeEditor.getValues();
        if(values.name == "" && values.tags==""){
           alert("Please enter name and tags");
           return;
        }
        dom.byId("createSnip_wrappedSubmit").firstChild.disabled=true;
        requestMgr.submitRequest("SAVE", values, this.saveSnippetCallback.closure(this),"Saving...");
    },

    saveSnippetCallback : function(results, msg){
        if(results.result){
            alert("Snippet saved successfully");
            this.tabbedPane._removeTab("Create Snippet");
            this.tabbedPane._selectTab("Browse snippets");
            this.browseTags.reRender();
            this.browseSnippets.reRender();

        }else{
            alert("An error occurred");
        }

       
    },
	
	deleteSnippetCallback: function(results){
		if(results.result){
            alert("Snippet deleted successfully");
            this.tabbedPane._removeTab("Create Snippet");
            this.tabbedPane._selectTab("Browse snippets");
            this.browseTags.reRender();
            this.browseSnippets.reRender();

        }else{
            alert("An error occurred");
        }
	},

    tagClickHandler:function(value){
        if(!this.tabbedPane.hasTab("By Tags:"+value)){
            var snippetResults = new SnippetResults({
                id:'browseByTag_'+value, cssStyle:"padding-top:10px",
                resultsFetcher:snippetz.tagResultsFetcher.closure(snippetz,value)
            });

            this.tabbedPane.addTab({name:"By Tags:"+value, widget:snippetResults}, true, true);
        } else{
            this.tabbedPane._selectTab("By Tags:"+value);
        }
    },


    tagResultsFetcher : function(value, beginIndex, endIndex, callback){
            requestMgr.submitRequest("SEARCH", {"beginIndex":beginIndex, "endIndex":endIndex, "keyword":value}, this.resultsFetcherCallback.closure(this, callback));
    },


    openAboutTab : function(){
        if(!this.tabbedPane.hasTab("About Snippy")) {
            var abtTab = new Widget({id:"aboutSnip",
                html:"<div class='about'> <strong>Snippy</strong> is a simple application developed " +
                     "in my free time, which can help you manage your code snippets at one place, with easy support for tagging. " +
                     "<p>Currently, it supports syntax-highlighting for <em>Javascript, C, C++, Java, Python, Perl, PHP, Ruby, Objective C and Scala</em> snippets. " +
                     "I do plan to add more languages support as I get time. I hope it is useful to somebody.</p> " +
                     "It is still very rough and you may find issues. Please report any issues, feedback or compliments(;))" +
                     " at <a href='mailto:vj.mysnippetz@gmail.com'>vj.mysnippetz@gmail.com</a></div>"});
            this.tabbedPane.addTab({name:"About Snippy", widget:abtTab}, true, true);
        }
        else{
            this.tabbedPane._selectTab('About Snippy');
        }
    },



    searchHandler:function(){
        var value = core.trim(core.findWidget("searchBarInput").getValue());
        if(value != ""){
            var values = value.split(" ");
            for(var i=0;i<values.length;i++){
                this.tagClickHandler(values[i]);
            }
        }

    },

    enterKeyListener:function(keyEvt){
        if(keyEvt.keyCode == 13){
            this.searchHandler();
        }
    }



};

