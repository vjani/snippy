/**
 * The SnippetResults widget represents the content in any of the browseSnippets/ByTags/browseTags tabs
 * in the application. It fetches the snippets based on the given criteria and displays them as a list.
 * If used as a tagUI, it fetches all the tags in the system and shows them as a tag cloud.
 * It also supports pagination.
 */
var SnippetResults = Class.define({
    name:"SnippetResults",

    extend:Widget,

    fields:{
        keyword:null,
        _resultsFetcher:null,
        _tagClickHandler:null,
        _fetchTags:false,
        _currentSetSize:0,
        _pageSize:5,
        _beginIndex:0,
        _endIndex:6
    },

    init : function(args) {
        Widget.superclass.call(this, args);
        if (args.keyword) {
            this._keyword = args.keyword;
        }
        if (args.resultsFetcher != null) {
            this._resultsFetcher = args.resultsFetcher;
        }
        if(args.fetchTags != null){
            this._fetchTags = args.fetchTags;
        }
        if(args.tagClickHandler != null){
            this._tagClickHandler = args.tagClickHandler;
        }

    },

    methods:{
        _render : function(elem) {

            elem.style.position = "relative";
            css.plusClass(elem, "snipRes");

            if (this._resultsFetcher) {
                var func = this._resultsFetcher.closure(this, this._beginIndex, this._endIndex, this._resultsCallback.closure(this));
                func();
            }
        },

        _resultsCallback : function(results) {
            if (results.result) {
                var result = results.result.snips;
                this._currentSetSize = results.result.snips? results.result.snips.length:0;

                var pageBar = dom.elem("div");

                var prevPage = dom.elem("a", {href:"#"});
                prevPage.innerHTML = 'Previous ' + this._pageSize + ' snippets';
                evt.addEventListener(prevPage, "click", this._prevPage.closure(this), false);
                if (this._beginIndex != 0) {
                    prevPage.style.visibility = 'visible';
                } else {
                    prevPage.style.visibility = 'hidden';
                }
                pageBar.appendChild(prevPage);


                if(this._currentSetSize >this._pageSize){
                    var nextPage = dom.elem("a", {href:"#", klass:"nextPageLink"});
                    nextPage.innerHTML = 'Next '+this._pageSize+' snippets';
                    evt.addEventListener(nextPage, "click", this._nextPage.closure(this), false);
                    pageBar.appendChild(nextPage);
                }


                var container = this.getContainer();
                container.appendChild(pageBar);
                if(!this._fetchTags) {
                    if(result.length == 0){
                        this._generateEmptyText();
                        return;
                    }
                    var resultList = dom.elem("ul", {klass:"snippetList"});

                    var len = result.length==this._pageSize+1?this._pageSize:result.length;
                    for (var i = 0; i < len; i++) {
                        var li = this._buildSnippet(result[i]);
                        resultList.appendChild(li);
                    }

                    container.appendChild(resultList);
                } else {
                    this._renderTagsUI(results);
                }
            } else {
                var div = dom.elem("div");
                div.innerHTML = "An error occurred";
                container.appendChild(div);
            }

        },
        
        _buildSnippet: function(snippet){
            var li = dom.elem("li");

            var nameSpan = dom.elem('span', {klass:"snippetName"});
            nameSpan.innerHTML = snippet.name;

            var dateSpan = dom.elem('span', {klass:"snippetDate"});
            dateSpan.innerHTML = snippet.postedDate;
            
            var langSpan = dom.elem('span',{klass:"snippetLang"});
            langSpan.innerHTML = "Language : "+snippet.type;
            
            var deleteSnip = dom.elem('a', {href: "#", klass:"snippetDelete"});
            deleteSnip.innerHTML = "Delete";
            evt.addEventListener(deleteSnip,"click", this._deleteSnip.closure(this, snippet.key), false);

            var tagDiv = dom.elem('div', {klass:"snippetTags"});
            var tags = [];
            if (snippet.tags && snippet.tags.length > 0) {
                for (var j = 0; j < snippet.tags.length; j++) {
                    /*if(results.result.tags)
                        tags.push(results.result.tags[result[i].tags[j]]);
                    else{
                        tags.push(snippetz._cachedTags[result[i].tags[j]].name);
                    }*/
                    tags.push(snippet.tags[j].name);
                }
                tags.sort();
                tagDiv.innerHTML = "Tags : "+ tags.join(", ");
            }

            var codePre = new CodeEditor({
                id:this.id+snippet.key+"_code",
                code:snippet.code,
                type:snippet.type
            });


            li.appendChild(nameSpan);
            li.appendChild(dateSpan);
            li.appendChild(deleteSnip);
            li.appendChild(tagDiv);
            li.appendChild(langSpan);
            codePre.appendTo(li);
            
            return li;
        },

        _generateEmptyText:function(){
            var emptyDiv = dom.elem("div", {style:"padding-left:20px"});

            emptyDiv.innerHTML = "Sorry! No results found. You may add a new Snippet using the 'Create Snippet' link at the top." ;
            this.getContainer().appendChild(emptyDiv);
        },

        _nextPage:function(){
            if(this._currentSetSize > this._pageSize){
                this._beginIndex += this._pageSize;
                this._endIndex += this._pageSize;
                this.reRender();
            }
        },

        _prevPage:function(){
            if(this._beginIndex != 0){
                this._beginIndex -=this._pageSize;
                this._endIndex -=this._pageSize;
                this.reRender();
            }
        },

        /**
         * Renders the tag cloud. The size of a tag is directly proportional to the
         * number of snippets that have been tagged using that tag.
         * @param results
         */
        _renderTagsUI:function(results){
            var tags = results.result;
            var container = this.getContainer();

            var divContainer = dom.elem("div");
            tags.sort(function(a,b){
                return a.name.toLowerCase()>b.name.toLowerCase()?1
                    :(a.name.toLowerCase()<b.name.toLowerCase()?-1:0);
            });

            if (tags.length > 0) {
                var max = tags[0].size;
                var min = tags[0].size;
                for (var j = 1; j < tags.length; j++) {
                    if (max < tags[j].size)
                        max = tags[j].size;
                    if (min > tags[j].size)
                        min = tags[j].size;
                }
            }

            for(var i=0;i<tags.length;i++){

               var span = dom.elem("span");
               var size = tags[i].size;
               var fontSize = 10;//min font size
               if(size > min){
                   fontSize = (40*(size- min))/(max-min);
               }
               fontSize = fontSize<10?10:fontSize;
               span.style.fontSize = fontSize+"px";
               span.title = tags[i].size + " snippets tagged";
               span.innerHTML = tags[i].name;
               css.plusClass(span, "tagCloud");

               divContainer.appendChild(span);
                if (i != 0 && i % 10 == 0) {
                  // divContainer.appendChild(dom.elem("br"));
               }
            }
            if(this._tagClickHandler){
                evt.addEventListener(divContainer, "click", this._clickHandler.closure(this), false);
                evt.addEventListener(divContainer, "mouseover", this._mouseOverHandler.closure(this), false);
                evt.addEventListener(divContainer, "mouseout", this._mouseOutHandler.closure(this), false);
            }
            container.appendChild(divContainer);
        },

        _clickHandler:function(clickEvt){
            if(evt.getTargetElem(clickEvt).tagName == 'SPAN'){
                var value = evt.getTargetElem(clickEvt).innerHTML;
                this._tagClickHandler(value);
            }
        },

        _mouseOverHandler:function(mouseEvt){
            var target = evt.getTargetElem(mouseEvt);
              if(target.tagName == 'SPAN'){
                target.style.color ='black';
                target.style.cursor='pointer';
                target.style.textDecoration='underline';
            }
        },
        _mouseOutHandler:function(mouseEvt){
            var target = evt.getTargetElem(mouseEvt);
              if(target.tagName == 'SPAN'){
                target.style.color ='black';
                target.style.cursor='none';
                target.style.textDecoration='none';
            }
        },
        
        _deleteSnip: function(id){
            if(confirm("Are you sure you want to delete this snippet?")){
                requestMgr.submitRequest("DELETE",{id:id}, snippetz.deleteSnippetCallback.closure(snippetz),
                                         "Deleting...");
            }
        },
        
        errorCallback: function(text){
            alert(text);
        }
    }
});
