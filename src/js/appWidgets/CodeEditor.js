/**
 * CodeEditor widget is used for either editing the code
 * along with name, tagList, type and code fields. Or In read-only mode, it is
 * used to display the snippet's details including syntax-highlighted code.
 */
var CodeEditor = Class.define({
    name:"CodeEditor",
    extend:Widget,
    fields:{
        isEdit:false,
        rawCode:null,
        count:0,
        type:"js",
        langs :[{label:"Javascript", value:"Javascript"},
                {label:"C", value:"C"},
                {label:"C++", value:"C++"},
                {label:"Java", value:"Java"},
                {label:"Python", value:"Python"},
                {label:"Perl", value:"Perl"},
                {label:"PHP", value:"Php"},
                {label:"Ruby", value:"Ruby"},
                {label:"Objective C", value:"Objective C"},
                {label:"Scala", value:"Scala"}
            ]
    },

    init:function(args){
        CodeEditor.superclass.call(this, args);
        if(args.isEdit != null){
            this.isEdit = args.isEdit;
        }
        if(args.code != null){
            this.rawCode = args.code;
        }
        if(args.type != null){
            this.type = args.type;
        }
    },

    methods:{
        _render : function(elem){
            var codePre;
            if(this.isEdit){
                var attrs = {
                    id: this.getSubId("code"),
                    rows: 28,
                    klass:"codeTextArea",
                    contentEditable : true,
                    style :"height:600px"
                    
                }
                codePre = dom.elem("textarea", attrs);
             
                codePre.value = this.rawCode;
            }
            else{
                var attrs = {
                    id : this.getSubId("code")
                }
                codePre = dom.elem("div", attrs);
                codePre.innerHTML = "<pre class='codePre'>"+ highlighter.format({code:this.rawCode, type: this.type})+ "</pre>";
            }
            /*if(this.isEdit){
                codePre.contentEditable = true;
            }*/

            if(this.isEdit) {

                var nameField = new InputText({
                    id:this.getSubId("name"),
                    innerLabel:"Name of snippet"
                });

                var tagsField =  new InputText({
                    id:this.getSubId("tags"),size:30,
                    innerLabel:"Comma(,) separated list of tags"
                });

                var typeField = new InputSelect({
                    id:this.getSubId("type"),
                    innerLabel:"Language",
                    values:this.langs
                });
                
               
                var submitButton = dom.elem("input", {type:"button", value:"Save Snippet"});
              
                evt.addEventListener(submitButton, "click", snippetz.saveSnippet.closure(snippetz), false);
                var wrappedSubmit = new Widget({
                    id:this.getSubId("wrappedSubmit"),
                    node:submitButton
                });

                

                var layout = new Horizontal({
                    id:this.getSubId("layout"), cssClass:"toolbarLayout",
                    children:[nameField, tagsField, typeField, wrappedSubmit]
                });

                var div = dom.elem("div");
                layout.appendTo(div);


                elem.appendChild(div);
            }
            elem.appendChild(codePre);
            
        },


        getValues : function(){
           var values = {};
           values.name = core.findWidget(this.id+"_name").getValue().trim();
           values.type = core.findWidget(this.id+"_type").getValue().trim();
           if(this.isEdit)
            values.code = dom.byId(this.id+"_code").value;
           values.tags = core.findWidget(this.id+"_tags").getValue().trim().replace(/\s+/g,"").split(",");
           //Put one of the tags as the language so that it is easy to search.
           values.tags.push(values.type);
           return values;
        }
    }
});
