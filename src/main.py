
from google.appengine.ext import webapp
from google.appengine.ext.webapp import util
from google.appengine.ext import db
from google.appengine.api import users
from django.utils import simplejson
from google.appengine.ext.webapp import template

import logging
import json
import model
import datetime
import os



class MainHandler(webapp.RequestHandler):

    def get(self):
        user = users.get_current_user()
       

        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'

            args = {
                'user': user.email(),
                'url':url,
                'url_linktext':url_linktext
            }
            path = os.path.join(os.path.dirname(__file__), './html/index.html')

            self.response.out.write(template.render(path, args))
        else:
            self.redirect(users.create_login_url(self.request.uri))

class RPCHandler(webapp.RequestHandler):

    def post(self):
        user = users.get_current_user()

        if user:
            action =  self.request.get("action", "")
            actParams = str(self.request.get("actionParams",""))
            logging.warn("actParams:"+actParams)
            actionParams = simplejson.loads(actParams)
           

            if action == 'BROWSE':
                result = self.browse(actionParams)
            elif action == 'SEARCH':
                result = self.search(actionParams)
            elif action == 'SAVE':
                result = self.save(actionParams)
            elif action == 'LOAD':
                result = self.load(actionParams)
            elif action == 'DELETE':
                result = self.deleteSnips(actionParams)
            elif action == 'BROWSE_TAGS':
                result = self.browseTags(actionParams)
            elif action == 'LOAD_TAGS':
                result = self.loadTags(actionParams)
            else:
                result = None
            self.response.headers['Content-Type'] = 'application/json; charset=utf-8'
            self.response.out.write(result)

        else:
            self.redirect(users.create_login_url(self.request.uri))


    ##################  RPC handlers ##################

    def browse(self, actionParams):

        beginIndex =  actionParams[u'beginIndex']
        endIndex = actionParams[u'endIndex']
        result = model.Snippet.gql("where user = :1 order by postedDate desc limit "+ str(beginIndex)+", "+str(endIndex),
                             users.get_current_user()).run()
        return simplejson.dumps({"result":{"snips":self.__convertSnippets__(result), "tags":[]}}, cls=json.GqlEncoder)

    def search(self, actionParams):

        beginIndex =  actionParams[u'beginIndex']
        endIndex = actionParams[u'endIndex']
        tags = model.Tag.gql("where name = '"+actionParams[u'keyword']+"' limit "
        +str(beginIndex)+", "+str(endIndex)).run()
        result = []
        for t in tags:
            logging.warn(t.name)
            snips = t.snippets
            for s in snips:
                result.append(s)
        return simplejson.dumps({"result":{"snips":self.__convertSnippets__(result), "tags":[]}}, cls=json.GqlEncoder)


    def save(self, actionParams):
        """Method to save the code snippets"""
        name = actionParams[u'name']
        type = actionParams[u'type']
        code = actionParams[u'code']
        tags = actionParams[u'tags']
        finalTags = []

        result = model.Tag.gql("where name in :names", names=tags).run()

        for tag in result:
            tags.remove(tag.name)
            finalTags.append(tag.key())

        for tag in tags:
            t = model.Tag(name=tag)
            t.put()
            finalTags.append(t.key())

        s = model.Snippet(name=name, type=type, code=code, user=users.get_current_user(), postedDate=datetime.datetime.now())
        s.put()

        for tag in finalTags:
            ts = model.TaggedSnippet(tag=tag, snippet=s)
            ts.put()


        return simplejson.dumps({"result":"success"})

    def load(self, actionParams):
        return None

    def deleteSnips(self, actionParams):
        id = actionParams[u'id']
        snippet = model.Snippet.get_by_id(id)
        #Find tagged snippet entries for this snippet
        taggedSnippets = model.TaggedSnippet.gql("where snippet = :snip", snip=snippet).run()
        
        #Find tags for this snippet.
        tags = snippet.tags
        
        #Delete taggedSnippets
        for ts in taggedSnippets:
            ts.delete()
        
        #If snippet count is 0, delete tags.
        for tag in tags:
            if tag.countSnippets == 0:
                tag.delete()
        
        snippet.delete()
        return simplejson.dumps({"result":"success"})

    def browseTags(self, actionParams):
        tags = model.Tag.all()
        return simplejson.dumps({"result":self.__convertTags__(tags)}, cls=json.GqlEncoder)


    def loadTags(self, actionParams):
        keys = actionParams.keys
        actualKeys =[]

        for key in keys:
            k = db.Key(key)
            actualKeys.append(k)

        tags = model.Tag.gql("where :p.contains(key)", actualKeys)
        result = self.__convertTags__(tags)

        return json.encode({"result":result})

    ##################  Helpers ##################

    def __convertTags__(self,tags):
        result = []
        for tag in tags:
            result.append({"name":tag.name, "key":tag.key().id(), "size":tag.countSnippets})
        return result

    def __convertSnippets__(self, snippets):
        result = []
        for snip in snippets:
            result.append({"name":snip.name, "key":snip.key().id(), "tags":snip.tags,
                           "type":snip.type,
                           "postedDate":snip.postedDate.strftime("%Y-%m-%d %H:%M ")+" UTC",
                           "code":snip.code})
        return result


def main():
    application = webapp.WSGIApplication([('/', MainHandler),
                                          ('/snippetz',RPCHandler)],
                                         debug=True)
    util.run_wsgi_app(application)


if __name__ == '__main__':
    main()
