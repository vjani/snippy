
from google.appengine.ext import db

class Tag(db.Model):
    name = db.StringProperty(required=True)

    @property
    def snippets(self):
        return [x.snippet for x in self.taggedsnippet_set]

    @property
    def countSnippets(self):
        return len(self.snippets)

class Snippet(db.Model):
    name = db.StringProperty(required=True)
    type = db.StringProperty(required=True)#, choices=set(['js']))
    postedDate = db.DateTimeProperty()
    code = db.TextProperty()
    user = db.UserProperty()

    @property
    def tags(self):
        return [x.tag for x in self.taggedsnippet_set]

class TaggedSnippet(db.Model):
    tag = db.ReferenceProperty(Tag)
    snippet = db.ReferenceProperty(Snippet)
