#! /bin/bash


rm -rf build
mkdir build
mkdir build/js

utilFiles[0]='js/util/core.js'
utilFiles[1]='js/util/json2.js'
utilFiles[2]='js/util/Class.js'
utilFiles[3]='js/util/highlighter.js'

widgetFiles[0]='js/widgets/Widget.js'
widgetFiles[1]='js/widgets/Horizontal.js'
widgetFiles[2]='js/widgets/InputText.js'
widgetFiles[3]='js/widgets/InputSelect.js'

widgetFiles[4]='js/widgets/Tabber.js'

appWidgetFiles[0]='js/appWidgets/CodeEditor.js'
appWidgetFiles[1]='js/appWidgets/SnippetResults.js'

appFiles[0]='js/app/snippetz.js'

pyFiles='*.py'


for i in ${utilFiles[*]}
do
	echo -e "\n/****************************** File : $i************************************/\n" >> build/js/snippy.js
	cat $i >> build/js/snippy.js
done

for i in ${widgetFiles[*]}
do
	echo -e "\n/****************************** File : $i************************************/\n" >> build/js/snippy.js
	cat $i >> build/js/snippy.js
done

for i in ${appWidgetFiles[*]}
do
	echo -e "\n/****************************** File : $i************************************/\n" >> build/js/snippy.js
	cat $i >> build/js/snippy.js
done

for i in ${appFiles[*]}
do
	echo -e "\n/****************************** File : $i************************************/\n" >> build/js/snippy.js
	cat $i >> build/js/snippy.js
done

cp *.py build/
cp *.yaml build/
cp *.html build/
cp -r css build/
cp -r html build/
cp -r images build/

